﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Roulette.Domain.Player;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Persistance.EntityConfigurations
{
    public class PlayerConfiguration : IEntityTypeConfiguration<Player>
    {
        public void Configure(EntityTypeBuilder<Player> builder)
        {
            //Table name
            builder.ToTable("Players");

            //required fields
            builder.Property(e => e.Username).IsRequired();


            //Unique Fields
            builder.HasIndex(p => p.Username).IsUnique();

            //Concurency check
            builder.Property(p => p.Balance).IsRowVersion().IsConcurrencyToken();
        }
    }
}
