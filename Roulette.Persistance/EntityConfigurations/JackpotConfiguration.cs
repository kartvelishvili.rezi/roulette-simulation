﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Roulette.Domain.Game;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Persistance.EntityConfigurations
{
    public class JackpotConfiguration : IEntityTypeConfiguration<Jackpot>
    {
        public void Configure(EntityTypeBuilder<Jackpot> builder)
        {
            //table name
            builder.ToTable("Jackpots");


            //required fields
            builder.Property(e => e.Name).IsRequired();

            //Unique Fields
            builder.HasIndex(p => p.Name).IsUnique();


            //Concurency
            builder.Property(p => p.CurrentValue).IsRowVersion().IsConcurrencyToken();
        }
    }
}
