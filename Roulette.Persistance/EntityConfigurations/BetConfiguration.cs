﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Roulette.Domain.Bet;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Persistance.EntityConfigurations
{
    public class BetConfiguration : IEntityTypeConfiguration<Bet>
    {
        public void Configure(EntityTypeBuilder<Bet> builder)
        {
            //Table name
            builder.ToTable("PlayerBets");

            //key
            builder.HasKey(e => e.Id);

            //Required Fields
            builder.Property(p => p.ValueAsString).IsRequired();
            builder.Property(p => p.CreatorIpAddress).IsRequired();

            //Unique Fields
            builder.HasIndex(p => p.SpinID).IsUnique();
        }
    }
}
