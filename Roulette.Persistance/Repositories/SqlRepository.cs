﻿using Microsoft.EntityFrameworkCore;
using Roulette.Application.Interfaces;
using Roulette.Domain.SeedWork;
using Roulette.Persistance.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.Persistance.Repositories
{
    public class SqlRepository<TEntity, TIdentity> : IRepository<TEntity>
      where TEntity : EntityBase<TIdentity>
    {
        protected readonly RouletteDbContext Context;

        public IUnitOfWork UnitOfWork => Context;
        public SqlRepository(RouletteDbContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<TEntity> AddAsync(TEntity aggregate)
        {
            var res = await Context.Set<TEntity>().AddAsync(aggregate);

            return res.Entity;
        }

        public Task RemoveAsync(TEntity aggregate)
        {
            Context.Entry(aggregate).State = EntityState.Deleted;

            return Task.CompletedTask;
        }


        public Task<TEntity> UpdateAsync(TEntity aggregate)
        {
            Context.Set<TEntity>().Update(aggregate);

            return Task.FromResult(aggregate);
        }


        public Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate = null)
        {
            return predicate == null ? 
                  Context.Set<TEntity>().ToListAsync()
                : Context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().SingleOrDefaultAsync(predicate);
        }

    }
}
