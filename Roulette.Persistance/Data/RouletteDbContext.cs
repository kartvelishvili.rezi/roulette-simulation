﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Roulette.Application.Interfaces;
using Roulette.Domain.Bet;
using Roulette.Domain.Game;
using Roulette.Domain.Player;
using Roulette.Persistance.EntityConfigurations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Roulette.Persistance.Data
{
    public class RouletteDbContext : DbContext, IRouletteDbContext, IUnitOfWork
    {
        public const string DefaultSchema = "RouletteSimulation";

        private IDbContextTransaction _currentTransaction;
        public RouletteDbContext(DbContextOptions<RouletteDbContext> options) : base(options)
        {
        }

        public DbSet<Player> Players { get; set; }

        public DbSet<Bet> Bets { get; set; }

        public DbSet<Jackpot> Jackpots { get; set; }

        public DbSet<Game> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BetConfiguration());
            modelBuilder.ApplyConfiguration(new PlayerConfiguration());
            modelBuilder.ApplyConfiguration(new JackpotConfiguration());
        }

        public async Task BeginTransactionAsync()
        {
            _currentTransaction ??= await Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);
        }

        public async Task RetryOnExceptionAsync(Func<Task> func)
        {
            await Database.CreateExecutionStrategy().ExecuteAsync(func);
        }

        public async Task CommitTransactionAsync()
        {
            try
            {
                await SaveChangesAsync();
                _currentTransaction?.Commit();
            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }
     
    }
}
