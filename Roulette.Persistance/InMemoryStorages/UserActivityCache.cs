﻿using Microsoft.Extensions.Caching.Memory;
using Roulette.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.Persistance.MemoryStorages
{
    public class UserActivityCache :IUserActivityCache
    {
        private readonly IMemoryCache _memoryCache;
        public UserActivityCache(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public Task AddOrUpdateAsync(Guid userId)
        {
            _memoryCache.Set(userId, DateTime.UtcNow);
            return Task.CompletedTask;
        }

        public Task<DateTime> GetUtcDateAsync(Guid userId)
        {
            return Task.FromResult((DateTime)_memoryCache.Get(userId));
        }

        public Task RemoveAsync(Guid userId)
        {
            _memoryCache.Remove(userId);
            return Task.CompletedTask;
        }
    }
}
