﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Roulette.API.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        private IHttpContextAccessor _accessor;
        public BaseController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }
        protected Task<string> GetClientIpAddress()
        {
            return Task.FromResult(_accessor.HttpContext.Connection.RemoteIpAddress.ToString());
        }

    }
}
