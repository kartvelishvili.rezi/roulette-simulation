﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Roulette.API.Infrastructure.Authorization;
using Roulette.Application.Commands.Betting;
using Roulette.Application.Queries;

namespace Roulette.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [RouletteAuthorize]
    [ServiceFilter(typeof(UserActivityAsyncActionFilter))]
    public class BetController : BaseController
    {
        private readonly IMediator _mediator;
        public BetController(IMediator mediator, IHttpContextAccessor accessor)
            :base(accessor)
        {
            _mediator = mediator;
        }

        [Route("Create")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create([FromBody]CreateBetCommand command)
        {
            var result = await _mediator.Send(new CreateBetCommand
            {
                BetAsString = command.BetAsString,
                ClientIpAddress = await GetClientIpAddress()
            });
            return Ok(result);
        }

        [Route("History")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> History([FromQuery] GetPlayerGameHistoryQuery query)
        {
            var result = await _mediator.Send(new GetPlayerBalanceQuery
            {
                PlayerId = query.PlayerId
            });
            return Ok(result);
        }
    }
}
