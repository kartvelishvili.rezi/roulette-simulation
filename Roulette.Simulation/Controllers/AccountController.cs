﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Roulette.API.Infrastructure.Authorization;
using Roulette.Application.Commands.Account;
using Roulette.Application.Queries;

namespace Roulette.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly IMediator _mediator;
        public AccountController(IHttpContextAccessor accessor, IMediator mediator)
            :base(accessor)
        {
            _mediator = mediator;
        }

        [Route("Login")]
        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Login([FromBody]SignInCommand command)
        {
            var result = _mediator.Send(new SignInCommand
            {
                Username = command.Username,
                Password = command.Password
            });
            return Ok(result);
        }

        [Route("Balance")]
        [HttpGet]
        [ServiceFilter(typeof(UserActivityAsyncActionFilter))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [RouletteAuthorize]
        public IActionResult UserBalance(GetPlayerBalanceQuery query)
        {
            var result = _mediator.Send(new GetPlayerBalanceQuery
            {
                PlayerId = query.PlayerId
            });
            return Ok(result);
        }
    }
}
