﻿using Autofac;
using Roulette.Application.Interfaces;
using Roulette.Application.Services;
using Roulette.Domain.Bet;
using Roulette.Domain.Game;
using Roulette.Domain.Player;
using Roulette.Persistance.MemoryStorages;
using Roulette.Persistance.Repositories;
using Roulette.Shared.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette.API.Infrastructure.AutofacModules
{
    public class ApplicationModule: Module
    {
        protected override void Load(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<SqlRepository<Bet, long>>()
                 .Named<IRepository<Bet>>(nameof(SqlRepository<Bet, long>))
                 .InstancePerLifetimeScope();

            containerBuilder.RegisterType<SqlRepository<Player, Guid>>()
                .Named<IRepository<Player>>(nameof(SqlRepository<Player, Guid>))
                .InstancePerLifetimeScope();

            containerBuilder.RegisterType<SqlRepository<Jackpot, int>>()
               .Named<IRepository<Jackpot>>(nameof(SqlRepository<Jackpot, int>))
               .InstancePerLifetimeScope();


            containerBuilder.RegisterType<JackpotAmountModificator>()
                .As<IJackpotAmountModificator>()
                .InstancePerLifetimeScope();
            containerBuilder.RegisterType<WinningNumberResolver>()
                .As<IWinningNumberResolver>()
                .InstancePerLifetimeScope();
            containerBuilder.RegisterType<JwtSecurityTokenGenerator>()
                .As<IJwtSecurityTokenGenerator>()
                .InstancePerLifetimeScope();

            containerBuilder.RegisterType<UserActivityCache>()
                .As<IUserActivityCache>()
                .InstancePerLifetimeScope();
        }
    }
}
