﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Roulette.Application.Interfaces;
using Roulette.Domain;
using Roulette.Domain.Player;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.API.Infrastructure.Authorization
{
    public class UserActivityAsyncActionFilter : IAsyncActionFilter
    {
        private readonly IUserActivityCache _userActivityCache;
        private readonly IRepository<Player> _playerRepository;
        private readonly IRouletteDbContext _context;
        public UserActivityAsyncActionFilter(
            IUserActivityCache userActivityCache, 
            IRepository<Player> playerRepository,
            IRouletteDbContext context
            )
        {
            _context = context;
            _userActivityCache = userActivityCache;
            _playerRepository = playerRepository;
        }

        public async Task OnActionExecutionAsync(
            ActionExecutingContext context,
            ActionExecutionDelegate next)
        {
            var userId = context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value; //parse userId
            if (string.IsNullOrEmpty(userId)) 
            {
               throw new UnauthorizedAccessException();
            }
            Guid playerId = Guid.Parse(userId);
            var lastRequestDate = await _userActivityCache.GetUtcDateAsync(playerId); // get last request date made by player
            if (lastRequestDate == null) // it's first request from player since he logged in
            {
                await _userActivityCache.AddOrUpdateAsync(playerId); 
                return;
            }
            var differenceInSeconds = (DateTime.UtcNow - lastRequestDate).TotalSeconds; // get the time difference between now and last request
            if (differenceInSeconds > RouletteConsts.PlayerDeactivationTimeInSeconds)
            {
                var player = await _playerRepository.GetAsync(e => e.Id == playerId);
                if (player == null)
                {
                    throw new KeyNotFoundException($"Couldn't find player with id {playerId}");
                }
                await _userActivityCache.RemoveAsync(playerId); // remove the player id from cache since player is going to be logged out
                player.ChangeSignature(); // change the signature which was encrypted in token, so previous token will become invalid
                await _playerRepository.UpdateAsync(player);
                await _playerRepository.UnitOfWork.SaveChangesAsync();
            }else
            {
                await _userActivityCache.AddOrUpdateAsync(playerId);
            }
            var resultContext = await next();
        }
    }


}
