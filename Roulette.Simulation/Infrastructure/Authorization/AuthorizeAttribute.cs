﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Roulette.Application.Interfaces;
using Roulette.Domain.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Roulette.API.Infrastructure.Authorization
{
    public class RouletteAuthorizeAttribute : TypeFilterAttribute
    {
        public RouletteAuthorizeAttribute() : base(typeof(ClaimRequirementFilter))
        {
        }
    }

    public class ClaimRequirementFilter : IAsyncAuthorizationFilter
    {
        private readonly IRepository<Player> _playerRepository;
        public ClaimRequirementFilter(IRepository<Player> playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (context.Filters.Any(item => item is IAllowAnonymousFilter))
            {
                return;
            }
            if (!context.HttpContext.User.Identity.IsAuthenticated)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
            var userId = context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var signature = context.HttpContext.User.FindFirst(ClaimTypes.Hash).Value;
            var user = await _playerRepository.GetAsync(u => u.Id == Guid.Parse(userId));
            if (user == null)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
            if (user.Signature != signature) //if the signature is changed user is unauthorized
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }
    }
}
