using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Roulette.API.Infrastructure.Authorization;
using Roulette.API.Infrastructure.AutofacModules;
using Roulette.API.Infrastructure.Filters;
using Roulette.Application.Hubs;
using Roulette.Application.Interfaces;
using Roulette.Application.MappingProfiles;
using Roulette.Persistance.Data;
using Roulette.Shared.Helpers.OptionsSettings;

namespace Roulette.Simulation
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services

                .AddCustomMvc()
                .AddCustomAutoMapper()
                .AddCustomDbContext(Configuration, Env)
                .AddCustomConfiguration(Configuration)
                .AddCustomSwagger()
                .AddSignalR();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new MediatorModule());
            builder.RegisterModule(new ApplicationModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<DashboardHub>("/dashboardhub");

            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

            });
            app.UseCors("CorsPolicy");

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }



    }

    internal class MySqlStorageExecutionStrategy : ExecutionStrategy
    {
        public MySqlStorageExecutionStrategy(ExecutionStrategyDependencies dependencies, int maxRetryCount, TimeSpan maxRetryDelay)
            : base(dependencies, maxRetryCount, maxRetryDelay)
        {

        }

        protected override bool ShouldRetryOn(Exception exception)
        {
            return true;
        }
    }
    internal static class CustomExtensionsMethods
    {
        public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment env)
        {
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<RouletteDbContext>(options =>
                {
                    options.UseMySQL(configuration["ConnectionString"], mySqlServerOptions =>
                    {
                        mySqlServerOptions.ExecutionStrategy(dependencies => new MySqlStorageExecutionStrategy(dependencies, 5, TimeSpan.FromMilliseconds(500)));
                    });
                }, ServiceLifetime.Scoped);

            services.AddScoped<IRouletteDbContext>(sp => sp.GetRequiredService<RouletteDbContext>());

            return services;
        }

        public static IServiceCollection AddCustomConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();
            services.Configure<TokenSettings>(configuration);
            services.AddScoped<UserActivityAsyncActionFilter>();
            return services;
        }

        public static IServiceCollection AddCustomAutoMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new BettingMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            return services;
        }

        public static IServiceCollection AddCustomMvc(this IServiceCollection services)
        {
            services.AddMvc();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            return services;
        }

        public static IServiceCollection AddCustomSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                });
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "RouletteSimulation HTTP API",
                    Version = "v1",
                    Description = "The RouletteSimulation Service HTTP API",
                });
                //options.OperationFilter<SecurityRequirementsOperationFilter>();
                //options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
            });

            return services;
        }

    }
}
