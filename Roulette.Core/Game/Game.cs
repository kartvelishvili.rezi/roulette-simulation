﻿using Roulette.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Domain.Game
{
    public class Game : EntityBase<int>
    {
        public Game(int winningNumber)
        {
            SpinId = Guid.NewGuid();
            WinningNumber = winningNumber;
        }

        public Guid SpinId { get; set; }

        public int WinningNumber { get; set; }

    }
}
