﻿using Roulette.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Domain.Game
{
    public class Jackpot : EntityBase<int>
    {
        public Jackpot()
        {

        }
        
        public string Name { get; set; }

        public long CurrentValue { get; set; }


        public void Increment(long amount)
        {
            CurrentValue += amount;
        }
    }
}
