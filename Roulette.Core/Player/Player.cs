﻿using Roulette.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Domain.Player
{
    public class Player : EntityBase<Guid>
    {
        public Player(string username, string password)
        {
            Username = username;
            Signature = Guid.NewGuid().ToString();
            Balance = 0;
        }
        public string Username { get; set; }

        public string PasswordHash { get; set; }

        public long Balance { get; set; }

        public string Signature { get; set; }


        public bool HasEnoughAmountForBet(long betAmount)
        {
            return betAmount <= Balance;
        }

        public bool WithDraw(long amount)
        {
            if (HasEnoughAmountForBet(amount))
            {
                Balance -= amount;
                return true;
            }
            return false;
        } 

        public bool MakeDeposit(long amount)
        {
            Balance += amount;
            return true;
        }
        public void ChangeSignature()
        {
            Signature = Guid.NewGuid().ToString();
        }

    }
}
