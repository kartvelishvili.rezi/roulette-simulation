﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Domain
{
    public static  class RouletteConsts
    {
        public static int BetAmountPercentageForJackpotMargin = 1;

        public static int PlayerDeactivationTimeInSeconds= 5 * 60;
    }
}
