﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Domain.SeedWork
{
    public abstract class EntityBase<TIdentity>
    {
        public EntityBase()
        {
            CreationTime = DateTime.UtcNow;
        }
        public TIdentity Id { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
