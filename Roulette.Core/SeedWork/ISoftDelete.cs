﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Domain.SeedWork
{
    public interface ISoftDelete
    {
        public bool IsDeleted { get; set; }
    }
}
