﻿using Roulette.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Domain.Bet
{
    public class Bet : EntityBase<long>
    {
        public Bet()
        {
            Status = BetStatus.Processing;
        }

        public BetStatus Status { get; set; }
        public string ValueAsString { get; set; }
        public long Amount { get; set; }
        public string CreatorIpAddress { get; set; }
    
        public Guid SpinID { get; set; }

        public long WonAmount { get; set; }
    
        public void Accept()
        {
            Status = BetStatus.Accepted;
        }   

        public void Reject()
        {
            Status = BetStatus.Rejected;
        }
    }
}
