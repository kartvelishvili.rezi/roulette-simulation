﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Domain.Bet
{
    public enum BetStatus
    {
        Processing,
        Accepted,
        Rejected
    }
}
