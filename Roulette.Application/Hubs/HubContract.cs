﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Application.Hubs
{
    //Contracts between client and server on a hub connection
    public struct HubContract
    {
        public struct Dashboard
        {
            public static string JackpotChanged = "roulette.simulation.jackpoint.changed";
        }
    }
}
