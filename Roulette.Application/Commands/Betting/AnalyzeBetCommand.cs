﻿using MediatR;
using Roulette.Application.Interfaces;
using Roulette.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Roulette.Application.Commands.Betting
{
    public class AnalyzeBetCommand : IRequest<CreateBetResultViewModel>, ITransactionalRequest
    {
        [DataMember]
        public string BetAsString { get; set; }

        public string ClientIpAddress { get; set; }
    }
}
