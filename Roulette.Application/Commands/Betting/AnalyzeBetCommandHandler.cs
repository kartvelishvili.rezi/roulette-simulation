﻿using ge.singular.roulette;
using MediatR;
using Roulette.Application.Interfaces;
using Roulette.Application.Services;
using Roulette.Application.ViewModels;
using Roulette.Domain;
using Roulette.Domain.Bet;
using Roulette.Domain.Game;
using Roulette.Domain.Player;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Roulette.Application.Commands.Betting
{
    public class AnalyzeBetCommandHandler
    {

        private readonly IRepository<Bet> _betRepository;
        private readonly IRepository<Jackpot> _jackPotRepository;
        private readonly IRepository<Player> _playerRepository;
        private readonly IWinningNumberResolver _winningNumberResolver;
        public AnalyzeBetCommandHandler(
            IRepository<Bet> betRepository,
            IRepository<Player> playerRepository,
            IRepository<Jackpot> jackPotRepository,
            IWinningNumberResolver winningNumberResolver
            )
        {
            _playerRepository = playerRepository ?? throw new ArgumentNullException(nameof(playerRepository));
            _jackPotRepository = jackPotRepository ?? throw new ArgumentNullException(nameof(jackPotRepository));
            _betRepository = betRepository ?? throw new ArgumentNullException(nameof(betRepository));
            _winningNumberResolver = winningNumberResolver ?? throw new ArgumentNullException(nameof(winningNumberResolver));
        }
        public async Task<AnalyzedBetViewModel> Handle(CreateBetCommand request, CancellationToken cancellationToken)
        {

            IsBetValidResponse ibvr = CheckBets.IsValid(request.BetAsString);

            if (!ibvr.getIsValid())
            {
                return new AnalyzedBetViewModel
                {
                    Status = BetStatus.Rejected
                };
            }
            var winningNumber = await _winningNumberResolver.GetWinnigNumberAsync();
            var game = new Game(winningNumber);
            long estWin = CheckBets.EstimateWin(request.BetAsString, winningNumber);
            long betAmount = ibvr.getBetAmount();


            //Check and update player
            var player = await _playerRepository.GetAsync(e => e.Id == request.PlayerId);
            if (player == null)
            {
                throw new KeyNotFoundException($"Couldn't find player with id {request.PlayerId}");
            }

            //with draw from player balance bet amount
            var withDrawSucceed = player.WithDraw(betAmount);
            if (!withDrawSucceed)
            {
                return new AnalyzedBetViewModel
                {
                    Status = BetStatus.Rejected
                };
            }
            //update player balance
            player.MakeDeposit(estWin);
            await _playerRepository.UpdateAsync(player);

            //Add bet 
            await _betRepository.AddAsync(new Bet
            {
                CreatorIpAddress = request.ClientIpAddress,
                SpinID = game.SpinId,
                ValueAsString = request.BetAsString,
                WonAmount = estWin,
                Amount = betAmount
            });

            //Update Jackpot Value
            var jackpot = await _jackPotRepository.GetAsync(e => e.Name == request.JackpotName);
            if (jackpot == null)
            {
                throw new KeyNotFoundException($"Couldn't find jackpot with name of {request.JackpotName}");
            }
            jackpot.Increment(betAmount * RouletteConsts.BetAmountPercentageForJackpotMargin / 100);
            await _jackPotRepository.UpdateAsync(jackpot);


            return new AnalyzedBetViewModel
            {
                SpinID = game.SpinId,
                WinningNumber = winningNumber,
                WonAmount = estWin,
                Status = BetStatus.Accepted,
                BetAmount = betAmount
            };
        }
    }
}
