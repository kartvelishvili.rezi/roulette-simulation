﻿using ge.singular.roulette;
using MediatR;
using Roulette.Application.Commands.Dashboard;
using Roulette.Application.ViewModels;
using Roulette.Domain.Bet;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Roulette.Application.Commands.Betting
{
    public class CreateBetCommandHandler : IRequestHandler<CreateBetCommand, CreateBetResultViewModel>
    {
        private readonly IMediator _mediator;
        public CreateBetCommandHandler(
            IMediator mediator
            )
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        public async Task<CreateBetResultViewModel> Handle(CreateBetCommand request, CancellationToken cancellationToken)
        {

            var analyzedBet = await _mediator.Send(new AnalyzeBetCommand
            {
                BetAsString = request.BetAsString,
                ClientIpAddress = request.ClientIpAddress
            });

            //if we got this far, bet was placed successfully and jackpot value was updated transactionally
            await _mediator.Send(new NotifyJackpotChangeCommand(request.JackpotName)); // notify clients 

            return new CreateBetResultViewModel
            {
                SpinID = analyzedBet.SpinID,
                WinningNumber = analyzedBet.WinningNumber,
                WonAmount = analyzedBet.WonAmount,
                Status = BetStatus.Accepted
            };
        }
    }
}
