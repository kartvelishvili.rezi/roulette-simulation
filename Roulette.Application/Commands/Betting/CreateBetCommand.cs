﻿using MediatR;
using Roulette.Application.Interfaces;
using Roulette.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Roulette.Application.Commands.Betting
{
    [DataContract]
    public class CreateBetCommand : IRequest<CreateBetResultViewModel>
    {
        [DataMember]
        public string BetAsString { get; set; }

        public Guid PlayerId { get; set; }

        public string ClientIpAddress { get; set; }

        public string JackpotName { get; set; }
    }
}
