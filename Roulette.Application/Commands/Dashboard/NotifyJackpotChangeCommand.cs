﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Application.Commands.Dashboard
{
    public class NotifyJackpotChangeCommand : IRequest
    {
        public string JackpotName { get; set; }

        public NotifyJackpotChangeCommand(string jackpotName)
        {
            JackpotName = jackpotName;
        }
    }
}
