﻿using MediatR;
using Microsoft.AspNetCore.SignalR;
using Roulette.Application.Hubs;
using Roulette.Application.Interfaces;
using Roulette.Application.ViewModels;
using Roulette.Domain.Game;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Roulette.Application.Commands.Dashboard
{
    public class NotifyJackpotChangeCommandHandler : AsyncRequestHandler<NotifyJackpotChangeCommand>
    {
        private readonly IHubContext<DashboardHub> _dashboardHub;
        private readonly IRepository<Jackpot> _jackpotRepository;
        public NotifyJackpotChangeCommandHandler(IHubContext<DashboardHub> dashboardHub, IRepository<Jackpot> jackpotRepository)
        {
            _dashboardHub = dashboardHub ?? throw new ArgumentNullException(nameof(dashboardHub)) ;
            _jackpotRepository = jackpotRepository ?? throw new ArgumentNullException(nameof(jackpotRepository));
        }
    
        protected override async Task Handle(NotifyJackpotChangeCommand request, CancellationToken cancellationToken)
        {
            var jackpot = await _jackpotRepository.GetAsync(e => e.Name == request.JackpotName);
            if (jackpot == null)
            {
                throw new KeyNotFoundException($"Couldn't find jackpot with name {request.JackpotName}");
            }
            await _dashboardHub.Clients.All.SendAsync(HubContract.Dashboard.JackpotChanged, new ChangeJackpotViewModel
            {
                Amount = jackpot.CurrentValue
            });
        }
    }
}
