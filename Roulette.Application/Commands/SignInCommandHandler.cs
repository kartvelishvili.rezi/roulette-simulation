﻿using MediatR;
using Roulette.Application.Interfaces;
using Roulette.Application.ViewModels;
using Roulette.Domain.Player;
using Roulette.Shared.Helpers;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Roulette.Application.Commands.Account
{
    public class SignInCommandHandler : IRequestHandler<SignInCommand, PlayerSignInResult>
    {
        private readonly IRepository<Player> _playerRepository;
        private readonly IJwtSecurityTokenGenerator _jwtSecurityTokenGenerator;
        public SignInCommandHandler(IRepository<Player> playerRepository, IJwtSecurityTokenGenerator jwtSecurityTokenGenerator)
        {
            _playerRepository = playerRepository ??throw new ArgumentNullException(nameof(playerRepository));
            _jwtSecurityTokenGenerator = jwtSecurityTokenGenerator ?? throw new ArgumentNullException(nameof(jwtSecurityTokenGenerator));
        }

        public async Task<PlayerSignInResult> Handle(SignInCommand request, CancellationToken cancellationToken)
        {
            //Find user with provided username and password
            var player = await _playerRepository.GetAsync(e => e.Username == request.Username && e.PasswordHash == PasswordHashier.Hash(request.Password));
            if (player == null)
            {
                    throw new Exception($"Username or password is incorrect!");
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, player.Id.ToString()),
                new Claim(ClaimTypes.Hash, player.Signature), // set signature in order to make token invalid by changing it
            };
            return new PlayerSignInResult
            {
                Token = _jwtSecurityTokenGenerator.Generate(claims),
                Id = player.Id,
  
            };
        }
    }
}
