﻿using MediatR;
using Roulette.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Roulette.Application.Commands.Account
{
    [DataContract]
    public class SignInCommand : IRequest<PlayerSignInResult>
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
