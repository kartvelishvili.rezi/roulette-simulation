﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.Application.Interfaces
{
    public interface IUserActivityCache
    {
        Task AddOrUpdateAsync(Guid userId);

        Task<DateTime> GetUtcDateAsync(Guid userId);

        Task RemoveAsync(Guid userId);
    }
}
