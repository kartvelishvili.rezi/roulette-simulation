﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.Application.Interfaces
{
    public interface IRouletteDbContext
    {
        Task BeginTransactionAsync();
        Task CommitTransactionAsync();
        void RollbackTransaction();
        Task RetryOnExceptionAsync(Func<Task> func);
    }
}
