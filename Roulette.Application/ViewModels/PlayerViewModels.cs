﻿using Roulette.Shared.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Application.ViewModels
{
    public class PlayerSignInResult : EntityDto<Guid>
    {
        public string Token { get; set; }


    }

    public class PlayerBalanceViewModel: EntityDto<Guid>
    {
        public double Balance { get; set; }
    }
}
