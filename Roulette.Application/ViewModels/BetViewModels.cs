﻿using Roulette.Domain.Bet;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Application.ViewModels
{
    public class CreateBetResultViewModel
    {
        public Guid SpinID { get; set; }

        public int WinningNumber { get; set; }

        public long WonAmount { get; set; }

        public BetStatus  Status { get; set; }
    }

    public class AnalyzedBetViewModel
    {
        public Guid SpinID { get; set; }

        public int WinningNumber { get; set; }

        public long WonAmount { get; set; }

        public BetStatus Status { get; set; }


        public long BetAmount { get; set; }
    }

    public class PlayerBetHistoryItemViewModel
    {
        public string SpinID { get; set; }

        public long BetAmount { get; set; }

        public long WonAmount { get; set; }

        public DateTime BetDate { get; set; }
    }
}
