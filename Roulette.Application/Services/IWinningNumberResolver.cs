﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.Application.Services
{
    public interface IWinningNumberResolver
    {
        Task<int> GetWinnigNumberAsync();
    }
}
