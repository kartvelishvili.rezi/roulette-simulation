﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.Application.Services
{
    public interface IJackpotAmountModificator
    {
        Task<int> Increment(int currentAmount, int betAmount);

        Task<int> GetIncrementAmount(int betAmount);
    }
}
