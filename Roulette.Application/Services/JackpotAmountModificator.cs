﻿using Roulette.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.Application.Services
{
    public class JackpotAmountModificator : IJackpotAmountModificator
    {
        public Task<int> GetIncrementAmount(int betAmount)
        {
            return Task.FromResult(betAmount * RouletteConsts.BetAmountPercentageForJackpotMargin / 100); // Calculate percentage of bet amount by given constant
        }

        public async Task<int> Increment(int currentAmount, int betAmount)
        {
            var margin = await GetIncrementAmount(betAmount);
            return currentAmount + margin;
        }
    }
}
