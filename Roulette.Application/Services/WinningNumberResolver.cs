﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Roulette.Application.Services
{
    public class WinningNumberResolver : IWinningNumberResolver
    {
        private readonly static int _rouletteMinValue = 0;
        private readonly static int _rouletteMaxValue = 36;
        public Task<int> GetWinnigNumberAsync()
        {
            Random random = new Random();
            int winningNumber = random.Next(_rouletteMinValue, _rouletteMaxValue);
            return Task.FromResult(winningNumber);
        }
    }
}
