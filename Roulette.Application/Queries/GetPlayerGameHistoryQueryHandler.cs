﻿using AutoMapper;
using MediatR;
using Roulette.Application.Interfaces;
using Roulette.Application.ViewModels;
using Roulette.Domain.Bet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Roulette.Application.Queries
{
    public class GetPlayerGameHistoryQueryHandler : IRequestHandler<GetPlayerGameHistoryQuery, List<PlayerBetHistoryItemViewModel>>
    {
        private readonly IRepository<Bet> _betRepository;
        private readonly IMapper _mapper;
        public GetPlayerGameHistoryQueryHandler(IRepository<Bet> betRepository, IMapper mapper)
        {
            _betRepository = betRepository ?? throw new ArgumentNullException(nameof(betRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<PlayerBetHistoryItemViewModel>> Handle(GetPlayerGameHistoryQuery request, CancellationToken cancellationToken)
        {
            var bets = await _betRepository.GetAllAsync();
            return bets.Select(e=> _mapper.Map< PlayerBetHistoryItemViewModel>(e)).ToList();
        }
    }
}
