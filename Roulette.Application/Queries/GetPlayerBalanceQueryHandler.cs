﻿using MediatR;
using Roulette.Application.Interfaces;
using Roulette.Application.ViewModels;
using Roulette.Domain.Player;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Roulette.Application.Queries
{
    public class GetPlayerBalanceQueryHandler : IRequestHandler<GetPlayerBalanceQuery, PlayerBalanceViewModel>
    {
        private readonly IRepository<Player> _playerRepository;
        public GetPlayerBalanceQueryHandler(IRepository<Player> playerRepository)
        {
            _playerRepository = playerRepository ??throw new ArgumentNullException(nameof(_playerRepository));
        }

        public async Task<PlayerBalanceViewModel> Handle(GetPlayerBalanceQuery request, CancellationToken cancellationToken)
        {
            var player = await _playerRepository.GetAsync(e => e.Id == request.PlayerId);
            return new PlayerBalanceViewModel
            {
                Balance = player.Balance,
                Id = player.Id
            };
        }
    }
}
