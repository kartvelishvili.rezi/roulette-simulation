﻿using MediatR;
using Roulette.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Roulette.Application.Queries
{
    [DataContract]
    public class GetPlayerGameHistoryQuery : IRequest<List<PlayerBetHistoryItemViewModel>>
    {
        [DataMember]
        public Guid PlayerId { get; set; }
    }
}
