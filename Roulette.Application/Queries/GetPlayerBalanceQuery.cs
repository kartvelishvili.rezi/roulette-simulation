﻿using MediatR;
using Roulette.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Application.Queries
{
    public class GetPlayerBalanceQuery: IRequest<PlayerBalanceViewModel>
    {
        public Guid PlayerId { get; set; }
    }
}
