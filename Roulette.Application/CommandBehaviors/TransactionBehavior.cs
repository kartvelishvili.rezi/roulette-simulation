﻿using MediatR;
using Roulette.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Roulette.Application.CommandBehaviors
{
    public class TransactionBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest
        : ITransactionalRequest
    {
        private readonly IRouletteDbContext _dbContext;

        public TransactionBehaviour(IRouletteDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentException(nameof(dbContext));
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            TResponse response = default;

            try
            {
                await _dbContext.RetryOnExceptionAsync(async () =>
                {
                    await _dbContext.BeginTransactionAsync();
                    response = await next();
                    await _dbContext.CommitTransactionAsync();

                });

                return response;
            }
            catch (Exception e)
            {
                _dbContext.RollbackTransaction();
                throw e;
            }
        }
    }
}
