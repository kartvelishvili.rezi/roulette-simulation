﻿using AutoMapper;
using Roulette.Application.ViewModels;
using Roulette.Domain.Bet;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Application.MappingProfiles
{
    public class BettingMappingProfile : Profile
    {
        public BettingMappingProfile()
        {
            CreateMap<Bet, PlayerBetHistoryItemViewModel>()
                .ForMember(m => m.BetAmount, opt => opt.MapFrom(s => s.Amount))
                .ForMember(m => m.BetDate, opt => opt.MapFrom(s => s.CreationTime));
        }
    }
}
