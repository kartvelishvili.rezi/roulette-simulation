﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Shared.Dtos
{
    public class EntityDto<T>
    {
        public T Id { get; set; }
    }
}
