﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Roulette.Shared.Helpers
{
    public  interface  IJwtSecurityTokenGenerator
    {
        string Generate(List<Claim> claims);
    }
}
