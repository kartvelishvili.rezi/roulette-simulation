﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Shared.Helpers.OptionsSettings
{
    public class TokenSettings
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string Key { get; set; }

        public int ExpirationTimeInMinutes { get; set; }
    }
}
